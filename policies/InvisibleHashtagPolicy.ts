import { NostrEvent, NostrRelayOK, NPolicy } from '@nostrify/nostrify';

export class InvisibleHashtagPolicy implements NPolicy {
  async call(event: NostrEvent): Promise<NostrRelayOK> {
    if (event.kind === 1) {
      const content = event.content.toLowerCase();

      for (const [name, value] of event.tags) {
        if (name === 't' && !content.includes(`#${value.toLowerCase()}`)) {
          return ['OK', event.id, false, 'blocked: invisible hashtag'];
        }
      }
    }

    return ['OK', event.id, true, ''];
  }
}
