import { MockRelay } from '@nostrify/nostrify/test';

import { CaptchaPolicy } from './CaptchaPolicy.ts';
import { assertEquals } from '@std/assert';

Deno.test('CaptchaPolicy', async () => {
  const store = new MockRelay();

  await store.event({
    'kind': 30382,
    'id': 'e8214ddd4e3345d420da9195b9133a7ca85764c446c0144c6f63526531eb8d5f',
    'pubkey': 'd058717dc414383ce870b21136aae566660afff5102d3f1c10796e7b60ecacfc',
    'created_at': 1728686282,
    'tags': [['d', '0461fcbecc4c3374439932d6b8f11269ccdb7cc973ad7a50ae362db135a474dd'], ['n', 'captcha_solved']],
    'content': '',
    'sig':
      'f4f971c50a9ce3fcedae84fa1452da6f44b2fd21fc4b0905d0f710ff9d7ee9430d5f099edc06b37a2222fe96fc5d72fba98e9187274c7b9b12125e0383c4295f',
  });

  const policy = new CaptchaPolicy(store, ['d058717dc414383ce870b21136aae566660afff5102d3f1c10796e7b60ecacfc']);

  const result = await policy.call({
    'kind': 1,
    'id': '62323bd7075aea329fc86de720a9119585c0476357489b53fd86c46d0eccc190',
    'pubkey': '0461fcbecc4c3374439932d6b8f11269ccdb7cc973ad7a50ae362db135a474dd',
    'created_at': 1728501370,
    'tags': [],
    'content': 'Olá mundo',
    'sig':
      '62447f6b0a469911df706fec29b9f4420d61f370015bdf011b01f6e3565809f99166da79f8bd7cf0e503b01cfd0358028a7051dfb94a801d07014a819cae3aac',
  });

  assertEquals(result, ['OK', '62323bd7075aea329fc86de720a9119585c0476357489b53fd86c46d0eccc190', true, '']);
});
