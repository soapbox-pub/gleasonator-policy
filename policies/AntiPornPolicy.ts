import { NostrEvent, NostrRelayOK, NPolicy } from '@nostrify/nostrify';
import { AnyPolicy, HashtagPolicy, KeywordPolicy } from '@nostrify/policies';

export class AntiPornPolicy implements NPolicy {
  async call(event: NostrEvent): Promise<NostrRelayOK> {
    const policy = new AnyPolicy([
      new KeywordPolicy(['https://']),
      new HashtagPolicy([
        'adult',
        'ass',
        'assworship',
        'boobs',
        'boobies',
        'butt',
        'cock',
        'dick',
        'dickpic',
        'explosionloli',
        'femboi',
        'femboy',
        'fetish',
        'fuck',
        'freeporn',
        'girls',
        'loli',
        'milf',
        'nude',
        'nudity',
        'nsfw',
        'pantsu',
        'pussy',
        'porn',
        'porno',
        'porntube',
        'pornvideo',
        'sex',
        'sexpervertsyndicate',
        'sexporn',
        'sexy',
        'slut',
        'teen',
        'tits',
        'teenporn',
        'teens',
        'transnsfw',
        'xxx',
      ]),
    ]);

    return policy.call(event);
  }
}
