import { NostrEvent, NostrRelayOK, NPolicy } from '@nostrify/nostrify';

export class LowercaseHashtagPolicy implements NPolicy {
  async call(event: NostrEvent): Promise<NostrRelayOK> {
    for (const [name, value] of event.tags) {
      if (name === 't' && value !== value.toLowerCase()) {
        return ['OK', event.id, false, 'blocked: uppercase letters in t tag'];
      }
    }

    return ['OK', event.id, true, ''];
  }
}
