import { MockRelay } from '@nostrify/nostrify/test';

import GleasonatorPolicy from './mod.ts';

Deno.test('GleasonatorPolicy', async () => {
  const store = new MockRelay();
  new GleasonatorPolicy({ store, pubkey: 'db0e60d10b9555a39050c258d460c5c461f6d18f467aa9f62de1a728b8a891a4' });
});
