import { NostrEvent, NostrRelayOK, NPolicy, NStore } from '@nostrify/nostrify';

/** Require users to have solved a Ditto captcha. */
export class CaptchaPolicy implements NPolicy {
  constructor(private store: NStore, private authors: string[]) {}

  async call(event: NostrEvent, signal?: AbortSignal): Promise<NostrRelayOK> {
    const [captcha] = await this.store.query([{
      kinds: [30382],
      authors: this.authors,
      '#d': [event.pubkey],
      '#n': ['captcha_solved'],
      limit: 1,
    }], { signal });

    if (captcha) {
      return ['OK', event.id, true, ''];
    } else {
      return ['OK', event.id, false, 'blocked: captcha required'];
    }
  }
}
