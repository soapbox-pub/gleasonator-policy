import { assertEquals } from '@std/assert';

import testEvent1 from '../fixtures/boring-text.json' with { type: 'json' };
import testEvent2 from '../fixtures/hentai-image.json' with { type: 'json' };
import testEvent3 from '../fixtures/pornvid.json' with { type: 'json' };
import testEvent4 from '../fixtures/political-link.json' with { type: 'json' };

import { AntiPornPolicy } from './AntiPornPolicy.ts';

Deno.test('AntiPornPolicy', async () => {
  // Content that should pass and be allowed
  assertEquals((await new AntiPornPolicy().call(testEvent1))[2], true);
  assertEquals((await new AntiPornPolicy().call(testEvent4))[2], true);

  // Content that should fail and be banned
  assertEquals((await new AntiPornPolicy().call(testEvent2))[2], false);
  assertEquals((await new AntiPornPolicy().call(testEvent3))[2], false);
});
