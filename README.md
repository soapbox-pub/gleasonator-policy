# Gleasonator Policy

This is the [Ditto policy](https://docs.soapbox.pub/ditto/policies/) used by gleasonator.dev.

To use it, just set `DITTO_POLICY="jsr:@gleasonator/policy"` in your Ditto env!
