import { NIP05, NostrEvent, NostrRelayOK, NPolicy, NStore } from '@nostrify/nostrify';
import {
  AntiDuplicationPolicy,
  AnyPolicy,
  DomainPolicy,
  FiltersPolicy,
  InvertPolicy,
  PipePolicy,
  ReplyBotPolicy,
  WoTPolicy,
} from '@nostrify/policies';

import { AntiPornPolicy } from './policies/AntiPornPolicy.ts';
import { CaptchaPolicy } from './policies/CaptchaPolicy.ts';
import { InvisibleHashtagPolicy } from './policies/InvisibleHashtagPolicy.ts';

import type { Kysely } from 'kysely';

const kv = await Deno.openKv();

const dittoPubkeys = [
  '15b68d319a088a9b0c6853d2232aff0d69c8c58f0dccceabfb9a82bd4fd19c58', // ditto.pub
  'db0e60d10b9555a39050c258d460c5c461f6d18f467aa9f62de1a728b8a891a4', // gleasonator.dev
  'fcbc1ad1784967e541b15614cfdd2766abe65c6990bcf0f8973bc527f75e5d27', // cobrafuma.com
  '13406302bf30a8a9495f5627293f8076768b08fdc5e4f024ce700f19e6289d27', // henhouse.social
];

export default class GleasonatorPolicy implements NPolicy {
  private policy: NPolicy;

  constructor(opts: { db: { kysely: Kysely<any> }; store: NStore; pubkey: string }) {
    const { db, store, pubkey } = opts;

    this.policy = new PipePolicy([
      new InvertPolicy(new FiltersPolicy([{ kinds: [31234] }]), 'blocked: draft events are blocked'),
      new InvisibleHashtagPolicy(),
      new AntiPornPolicy(),
      new AntiDuplicationPolicy({ kv, minLength: 10 }),
      new ReplyBotPolicy({ store }),
      new AnyPolicy([
        // Allow all kinds besides 1, 3, 4, 6, and 7 without further checks.
        new InvertPolicy(new FiltersPolicy([{ kinds: [1, 4, 6, 7] }]), 'blocked: posting is prohibited'),
        // The user has solved a captcha on this server or another trusted Ditto server.
        new CaptchaPolicy(store, [pubkey, ...dittoPubkeys]),
        // Allow npubs in Web of Trust.
        new WoTPolicy({
          store,
          pubkeys: [
            '82341f882b6eabcd2ba7f1ef90aad961cf074af15b9ef44a09f9d2a8fbfbe6a2', // jack
            '0461fcbecc4c3374439932d6b8f11269ccdb7cc973ad7a50ae362db135a474dd', // alex
          ],
          depth: 2,
        }),
        new DomainPolicy(store, {
          blacklist: [
            'spamstr.lol', // spam attack
            'bsky-social.eclipse.pub', // too much data to store affordably, rely on WoT to whitelist specific users
            'fsebugoutzone-org.mostr.pub', // spam attack
          ],
          lookup: async (nip05, signal) => {
            const row = await db.kysely
              .selectFrom('author_stats')
              .select('pubkey')
              .where('nip05', '=', nip05)
              .executeTakeFirst();

            if (row) {
              return { pubkey: row.pubkey };
            }

            return NIP05.lookup(nip05, { signal });
          },
        }),
      ]),
    ]);
  }

  async call(event: NostrEvent, signal?: AbortSignal): Promise<NostrRelayOK> {
    return this.policy.call(event, signal);
  }
}
